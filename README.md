# docker-hub-exporter

### Environment variables
* `APP_DOCKER_HUB_IMAGES` — comma-separated list of watching images. Example: `APP_DOCKER_HUB_IMAGES=user1/image1,user2/image2`;
* `APP_BIND_ADDR` — web-server address and port. Default value is `APP_BIND_ADDR=:8080`.
