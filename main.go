package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"strings"
	"sync"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

type repositoryInfoV2 struct {
	Namespace string `json:"namespace"`
	User string `json:"user"`
	Name string `json:"name"`
	Type string `json:"repository_type"`
	Status int `json:"status"`
	StarCount int `json:"star_count"`
	PullCount int `json:"pull_count"`
	LastUpdated time.Time `json:"last_updated"`
}

func (ri *repositoryInfoV2)Image() string {
	return ri.User + "/" + ri.Name
}

const (
	EnvDockerHubImages = "APP_DOCKER_HUB_IMAGES"
	EnvBindAddr = "APP_BIND_ADD"
	DefaultBindAddr = ":8080"
	PrometheusNamespace = "docker_hub"
	RequestTimeout = 10
	InfoUpdateInterval = 60
)

var (
	httpClient *http.Client
	info []repositoryInfoV2
)

func getRepositoryInfo(image string) (info repositoryInfoV2, err error) {
	log.Printf("Get image %q.", image)

	response, responseErr := httpClient.Get("https://hub.docker.com/v2/repositories/" + image)

	if responseErr != nil {
		err = responseErr
		return
	}

	if response.Body != nil {
		defer response.Body.Close()
	}

	if response.StatusCode != 200 {
		err = fmt.Errorf("wrong status code %d (%s)", response.StatusCode, response.Status)
		return
	}

	if decodeErr := json.NewDecoder(response.Body).Decode(&info); decodeErr != nil {
		err = decodeErr
		return
	}

	return
}

func init() {
	httpClient = &http.Client{
		Timeout: time.Duration(RequestTimeout) * time.Second,
	}

	images := strings.FieldsFunc(os.Getenv(EnvDockerHubImages), func(c rune) bool { return c == ',' })

	if len(images) == 0 {
		log.Panicf("Define %q environment variable.", EnvDockerHubImages)
	}

	log.Printf("Images: %s. Total: %d.", strings.Join(images, ", "), len(images))

	info = make([]repositoryInfoV2, len(images))
	ch := make(chan repositoryInfoV2)

	for _, image := range images {
		log.Printf("Init image %q.", image)

		go func(image string, ch chan repositoryInfoV2) {
			info, err := getRepositoryInfo(image)

			if err != nil {
				log.Panicf("Unable to get image %q: %s.", image, err)
			}

			ch <- info
		}(image, ch)
	}

	for index := range images {
		info[index] = <-ch
	}

	log.Println("All images are initialized.")
}

func update() {
	for {
		time.Sleep(InfoUpdateInterval * time.Second)
		log.Println("Update repositories info.")

		var wg sync.WaitGroup

		for index := range info {
			wg.Add(1)

			go func(index int, wg *sync.WaitGroup) {
				oldInfo := info[index]
				newInfo, err := getRepositoryInfo(oldInfo.Image())

				if err != nil {
					log.Printf("Error occured while getting info about image %q: %s.", oldInfo.Image(), err)
				} else {
					log.Printf("Info about image %q is updated.", oldInfo.Image())
					info[index] = newInfo

					if oldInfo.PullCount != newInfo.PullCount {
						log.Printf("Delta for %q (PullCount): %d → %d.", newInfo.Image(), oldInfo.PullCount, newInfo.PullCount)
					}

					if oldInfo.StarCount != newInfo.StarCount {
						log.Printf("Delta for %q (StarCount): %d → %d.", newInfo.Image(), oldInfo.StarCount, newInfo.StarCount)
					}

					if oldInfo.Status != newInfo.Status {
						log.Printf("Delta for %q (Status): %d → %d.", newInfo.Image(), oldInfo.Status, newInfo.Status)
					}

					if !oldInfo.LastUpdated.Equal(newInfo.LastUpdated) {
						log.Printf("Delta for %q (LastUpdated): %s → %s.", newInfo.Image(), oldInfo.LastUpdated, newInfo.LastUpdated)
					}
				}

				wg.Done()
			}(index, &wg)
		}

		wg.Wait()
	}
}

func main() {
	go update()

	for index := range info {
		func(index int) {
			constLabels := prometheus.Labels{
				"user":      info[index].User,
				"name":      info[index].Name,
				"repository_type": info[index].Type,
				"namespace": info[index].Namespace,
			}

			prometheus.Register(prometheus.NewCounterFunc(
				prometheus.CounterOpts{
					Namespace: PrometheusNamespace,
					Name:      "pull_count",
					Help:      "Count of image pulls.",
					ConstLabels: constLabels,
				},
				func() float64 { return float64(info[index].PullCount) },
			))

			prometheus.Register(prometheus.NewCounterFunc(
				prometheus.CounterOpts{
					Namespace: PrometheusNamespace,
					Name:      "star_count",
					Help:      "Count of image stars.",
					ConstLabels: constLabels,
				},
				func() float64 { return float64(info[index].StarCount) },
			))

			prometheus.Register(prometheus.NewGaugeFunc(
				prometheus.GaugeOpts{
					Namespace: PrometheusNamespace,
					Name: "status",
					ConstLabels: constLabels,
				},
				func() float64 { return float64(info[index].Status) },
			))

			prometheus.Register(prometheus.NewGaugeFunc(
				prometheus.GaugeOpts{
					Namespace: PrometheusNamespace,
					Name: "last_updated",
					Help: "Number of seconds since 1970 of last update.",
					ConstLabels: constLabels,
				},
				func() float64 { return float64(info[index].LastUpdated.Unix()) },
			))
		}(index)
	}

	bindAddr := os.Getenv(EnvBindAddr)

	if bindAddr == "" {
		bindAddr = DefaultBindAddr
	}

	log.Printf("Try to bind web-server on %q.", bindAddr)
	http.Handle("/metrics", promhttp.Handler())

	if err := http.ListenAndServe(bindAddr, nil); err != nil {
		log.Fatalf("Web-server stopped with error: %s.", err)
	}
}
