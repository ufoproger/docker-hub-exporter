FROM golang:1.14-alpine as builder

RUN apk add --no-cache git

WORKDIR /go
COPY main.go .

RUN go get -d -v
RUN go build -v -o docker-hub-exporter

FROM alpine:latest

LABEL maintainer="Mikhail Snetkov <ufoproger@gmail.com>"

COPY --from=builder /go/docker-hub-exporter /usr/local/bin/docker-hub-exporter

EXPOSE 8080

CMD ["/usr/local/bin/docker-hub-exporter"]
